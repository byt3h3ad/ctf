import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import supabase from "@/lib/supabase";
import { Toaster } from "@/components/ui/toaster";
import { useToast } from "@/components/ui/use-toast";
import { ToastAction } from "@/components/ui/toast";

const formSchema = z.object({
  Name: z.string({ required_error: "Name is required" }),
  ScholarID: z
    .string({ required_error: "Scholar ID is required" })
    .min(7, { message: "Invalid Scholar ID" }),
  Phone: z
    .string({ required_error: "Phone Number is required" })
    .length(10, { message: "Phone numbers are 10 digits long" }),
  Email: z
    .string({ required_error: "Email is required" })
    .email({ message: "Invalid email address" }),
});

export default function Success() {
  const { toast } = useToast();

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      Name: "",
      ScholarID: "",
      Phone: "",
      Email: "",
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    const { Name, ScholarID, Phone, Email } = values;
    try {
      const { error } = await supabase
        .from("completions")
        .insert({ Name, ScholarID, Phone, Email });
      if (error) {
        throw error;
      } else {
        toast({
          title: "Congratulations on successfully completing the challenge!",
          description: "The results will be declared shortly.",
        });
      }
    } catch (error) {
      toast({
        variant: "destructive",
        title: "The form could not be submitted.",
        action: <ToastAction altText="Try again">Try again</ToastAction>,
      });
      console.log("Error occured: ", { error });
    }
  }

  return (
    <main className="grid border-slate-600 place-items-center h-min md:p-16 m-8">
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="lg:w-1/3 space-y-6"
        >
          <FormField
            control={form.control}
            name="Name"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Name</FormLabel>
                <FormControl>
                  <Input placeholder="Your Name" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="ScholarID"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Scholar ID</FormLabel>
                <FormControl>
                  <Input placeholder="Scholar ID" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="Phone"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Phone number</FormLabel>
                <FormControl>
                  <Input placeholder="Phone" {...field} />
                </FormControl>
                <FormDescription>
                  Enter your 10-digit phone number.
                </FormDescription>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="Email"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Email address</FormLabel>
                <FormControl>
                  <Input placeholder="hello@example.com" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <Button type="submit">Submit</Button>
        </form>
        <Toaster />
      </Form>
    </main>
  );
}
